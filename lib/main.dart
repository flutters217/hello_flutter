import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _MystatefulWidgetState createState() => _MystatefulWidgetState();
}

String englishGreeting = "Hello Flutter";
String spanisGreeting = "Hola Flutter";
String frenchsGreeting = "Bonjour Flutter";
String portugueseGreeting = "Olá Flutter";
String italianGreeting = "Ciao Flutter";
String russiasGreeting = "Preevyet Flutter";
class _MystatefulWidgetState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
              onPressed:() {
                setState((){
                  displayText = displayText != spanisGreeting? spanisGreeting : englishGreeting;
                });
              },
              icon: Icon(Icons.accessibility)),
            IconButton(
                onPressed:() {
                  setState((){
                    displayText = displayText != frenchsGreeting? frenchsGreeting : italianGreeting;
                  });
                },
                icon: Icon(Icons.account_box_rounded)),
            IconButton(
                onPressed:() {
                  setState((){
                    displayText = displayText != portugueseGreeting? portugueseGreeting : russiasGreeting;
                  });
                },
                icon: Icon(Icons.refresh))
          ],

        ),
        body: Center(
          child: Text(
            displayText,style: TextStyle(fontSize: 26),
          ),
        ),
      )
    );
  }
}

















// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[IconButton(onPressed:() {},
//               icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text("Hello Flutter",style: TextStyle(fontSize: 24),
//           ),
//         ),
//       )
//     );
//   }
// }